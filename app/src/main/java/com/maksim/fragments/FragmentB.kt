package com.maksim.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_b.*


class FragmentB : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_b, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getInt(KEY_QUANTITY).let {
            tvFragmentB?.text = it.toString()
        }
    }

    companion object {
        fun newInstance(quantity: Int) = FragmentB().apply {
            arguments = Bundle().apply {
                putSerializable(KEY_QUANTITY, quantity)
            }
        }

        const val TAG_FRAGMENT_B = "TAG_FRAGMENT_B"
        val KEY_QUANTITY = "quantity"
    }
}
