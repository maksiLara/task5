package com.maksim.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_a.*


class FragmentA : Fragment() {

    private var listener: View.OnClickListener? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? View.OnClickListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_a, container, false)
    }

    override fun onStart() {
        super.onStart()
        btnFragmentA?.setOnClickListener { listener?.onClick(btnFragmentA) }
    }

    companion object {
        const val TAG_FRAGMENT_A = "TAG_FRAGMENT_A"
    }
}
