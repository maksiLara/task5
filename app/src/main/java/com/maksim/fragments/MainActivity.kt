package com.maksim.fragments

import android.content.res.Configuration
import android.graphics.drawable.GradientDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.View
import android.widget.Toast
import com.maksim.fragments.FragmentA.Companion.TAG_FRAGMENT_A
import com.maksim.fragments.FragmentB.Companion.TAG_FRAGMENT_B
import com.maksim.fragments.FragmentB.Companion.newInstance
import kotlinx.android.synthetic.main.fragment_a.*
/*
*This Activity used two fragment and it has a push counter
* FragmentA and button are located in portrait orientation,
* If i clicking on button my fragmentA is replaced by a FragmentB
* FragmentB has a push counter in TextView
* FragmentA and FragmentB are stored in landscape orientation
*
*@author Maksim Larchenko
*
 */

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var fragmentA: FragmentA? = FragmentA()
    var quantity: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        quantity = savedInstanceState?.getSerializable(KEY_QUANTITY) as Int? ?: 0
        when (resources.configuration.orientation) {
            Configuration.ORIENTATION_PORTRAIT -> addPortrait()
            Configuration.ORIENTATION_LANDSCAPE -> addLandScape()
        }
    }

    override fun onClick(v: View?) {
        quantity++
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            supportFragmentManager.beginTransaction().replace(R.id.container, createFragmentB(), TAG_FRAGMENT_B)
                .addToBackStack(null).commit()
        } else supportFragmentManager.beginTransaction().replace(R.id.containerB, createFragmentB(), TAG_FRAGMENT_B)
            .addToBackStack(null).commit()
    }

    fun addPortrait() {
        val fragmentManager = supportFragmentManager.findFragmentByTag(TAG_FRAGMENT_B) as? FragmentB
        supportFragmentManager.popBackStack()
        if (fragmentManager != null && quantity != 0) {
            fragmentManager.arguments?.putSerializable(KEY_QUANTITY, quantity)
            supportFragmentManager.popBackStack()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, createFragmentB(), TAG_FRAGMENT_B)
                .addToBackStack(null)
                .commit()
        } else supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragmentA!!, TAG_FRAGMENT_A)
            .commit()
    }

    fun addLandScape() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.containerA, FragmentA(),TAG_FRAGMENT_A)
            .commit()
        supportFragmentManager.beginTransaction()
            .replace(R.id.containerB, createFragmentB(), TAG_FRAGMENT_B)
            .addToBackStack(null)
            .commit()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putSerializable(KEY_QUANTITY, quantity)
        super.onSaveInstanceState(outState)
    }

    fun createFragmentB(): FragmentB {
        return FragmentB.newInstance(quantity)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (resources.configuration.orientation==Configuration.ORIENTATION_LANDSCAPE){
            finish()
        }
    }

    companion object {
        val KEY_QUANTITY = "quantity"
    }
}
